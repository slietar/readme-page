let fs = require('fs')
let MarkdownIt = require('markdown-it')

let read = (filename) => fs.readFileSync(filename).toString()

let [projectName, projectNamespace, commitNumber, inputFilename] = process.argv.slice(2)

let md = new MarkdownIt()
let html = md.render(read(inputFilename))
let template = read(__dirname + '/../template.html')

template = template
  .replace('{{contents}}', html)
  .replace('{{project-name}}', projectName)
  .replace('{{repo-url}}', `https://gitlab.com/${projectNamespace}`)
  .replace('{{downloads-url}}', `https://gitlab.com/${projectNamespace}/tags`)
  .replace('{{license-url}}', `https://gitlab.com/${projectNamespace}/blob/master/license.md`)
  .replace('{{commit-number}}', commitNumber)

process.stdout.write(template)

